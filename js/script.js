/*TextDecor- A CSS Generator For Text & Font Styling */
"use strict";
(function(){

    var s= new Service(),   /*using service.js*/
    w = window.innerWidth,
	text= $("#input").val(), /*initial value of input*/
	formElement= $('.form-element'),  /*all form elements*/
	selectElement= formElement.filter(function(index){
		return index>0;
	}),   /*all form elements except the first one which is the textarea*/

	/*Arrays of css properties*/
	fontArr=["'Fjalla One', sans-serif","'Lato', sans-serif",
    "'Open Sans', sans-serif","'Droid Sans', sans-serif","'Source Sans Pro', sans-serif","'Roboto', sans-serif","'Slabo 27px', serif","'Oswald', sans-serif","'Roboto Condensed', sans-serif","'Lora', serif",
    "'Montserrat', sans-serif","'PT Sans', sans-serif","'Raleway', sans-serif","'Open Sans Condensed', sans-serif","'Ubuntu', sans-serif","'Roboto Slab', serif","'Droid Serif', serif","'Merriweather', serif","'Arimo', sans-serif","'PT Sans Narrow', sans-serif"],
	sizeArr=[],
	styleArr=['normal','italic','oblique'],
	weightArr=['400','500','600','700','800','900','bold','bolder'],
	vrntArr=['normal','small-caps'],
	colorArr=['#000000'],
	spaceArr=[],
	transformArr= ['none','capitalize','lowercase','uppercase'],
	decorArr= ['none','underline','overline','line-through'],
	alignArr= ['initial','left','right','center','justify'],
	/***end of arrays of css properties***/
	cssProperties= $('.css-code').find('p'), /*all p's which contain the generated code*/
	size= $('#size'), 
	style= $('#style'),  /*select element which hold the font-size options*/
	font= $('#fonts'),
	weight= $('#weight'),
	variant= $('#variant'),
	color= $('#color'),
	background= $('#background'),
	lSpacing= $('#spacing'),
	wSpacing= $('#word-spacing'),
	transform= $('#transform'),
	decoration= $('#decoration'),
	align= $('#align'),
	padding= $('#padding'),
	notice= $('.notice').find('small'),  /*container of dynamic link text*/
	resetBtn= $('button#reset'),
	copyBtn= $('#copy'),
	randomBtn= $('#random'),
	addBtn= $('#add'),
	checkBox= $('.checkbox-common'),
	randomCheck= $('.random-check'),
	colorAddon= $('span.input-group-addon-color').find('span'),
	backgroundAddon= $('span.input-group-addon-background').find('span'),
	checkLabel= $('.check-label'),
	checkUncheck= $('#check-uncheck'),
	savedStyle= $('.saved-styles');

    text= text.replace(/(<([^>]+)>)/ig,""); /*strips all html tags and their contents*/
    color.colorpicker();        /*colorpicker for changing color*/
    background.colorpicker();   /*colorpicker for changing bg */
    disableElements();                /*disable elements at initial state*/
    cssProperties.hide();             /*hides css codes at initial state*/
    randomCheck.hide();
    /*pushing and manipulating property arrays*/
    s.indexPush(sizeArr,10,80);
    s.addString(sizeArr,"px");
    s.indexPush(spaceArr,0,40);
	s.addString(spaceArr,"px");
	s.arraySort(fontArr);
	/***end of pushing and manipulating property arrays***/
	notice.hide(); 
	linkGenerator(fontArr[0],'font-link'); /*initial link generation*/
	/*hides the colorpicker color*/
	colorAddon.hide(); 
	backgroundAddon.hide();
	checkLabel.html('Uncheck All');
	checkUncheck.attr('checked','checked');
	checkBox.attr('checked','checked');

    /*creates dynamic href attribute for font link*/
	function linkAttribute(str,id){
		var att = document.createAttribute("href");       
	    att.value = str;                           
	    document.getElementById(id).setAttributeNode(att);
	}

    /*dynamically creates google font links*/
	function linkGenerator(fontNameStr,id){
		if(typeof fontNameStr=== 'string'){
			var fontNameArr= fontNameStr.split(",");		
			var fontName= fontNameArr[0];
			fontName= fontName.trim().replace(/'+/g,"");
			var googleFont= "https://www.google.com/fonts#UsePlace:use/Collection:";
            $('.font-name').html("'"+fontName+"'");
			if(fontName.indexOf(" ")==-1){
			    var link= googleFont+fontName;
			    linkAttribute(link,id);
			}
			else{
			    var words= fontName.split(" ");
			    var linkWords= words.join("+");
			    var link= googleFont+linkWords;
			    linkAttribute(link,id);
			}
		}
		else{
			return;
		}
	}

    /*default styling of output*/
	function defaultStyle(){

		output.css({
		    'font-size': size.find('option').eq(4).html(),
		    'font-style': styleArr[0],
		    'font-family': fontArr[0],
		    'font-weight': weightArr[0],
		    'font-variant': vrntArr[0],
		    'color': colorArr[0],
		    'letter-spacing': spaceArr[0],
		    'word-spacing': spaceArr[0],
		    'text-transform': transformArr[0],
		    'text-decoration': decorArr[0],
		    'text-align': alignArr[0],
		    'background': '#fffcfc',
		    'padding':spaceArr[6]
		});
	}

    /*defalt style css code display*/ 
	function deafaulStyleDisplay(){

        $("#font-size-code").html(size.find('option').eq(4).html()+";");
	    $("#font-style-code").html(styleArr[0]+";");
	    $("#font-family-code").html(fontArr[0]+";");
	    $("#font-weight-code").html(weightArr[0]+";");
	    $("#font-variant-code").html(vrntArr[0]+";");
	    $("#color-code").html(colorArr[0]+';');
	    $("#letter-spacing-code").html(spaceArr[0]+";");
	    $("#word-spacing-code").html(spaceArr[0]+";");
	    $("#text-transform-code").html(transformArr[0]+";");
	    $("#text-decoration-code").html(decorArr[0]+";");
	    $("#text-align-code").html(alignArr[0]+";");
	    $("#background-code").html('#fffcfc'+';');
	    $("#padding-code").html(spaceArr[6]+';');
	}

    /*sets color in rgba format*/
	function colorSetter(colorId,property,colorDisplay,colorSpan){

        $(colorId).colorpicker().on('changeColor.colorpicker', function(event){
	        var red= event.color.toRGB().r,
	        green= event.color.toRGB().g,
	        blue= event.color.toRGB().b,
	        alpha= event.color.toRGB().a,
	        color= "rgba("+red+","+green+","+blue+","+alpha+")";
	        output.css(property,color);
	        $(colorDisplay).html(color+";");
	        $(colorSpan).find('span').show('700');
	        $(colorSpan).find('span').css({
	        	'background-color': color,
	        	'color':color
	        });
		});
	}

	function enableElements(){
		selectElement.removeAttr('disabled','disabled');
        resetBtn.removeAttr('disabled','disabled');
        copyBtn.removeAttr('disabled','disabled');
        randomBtn.removeAttr('disabled','disabled');
        checkBox.removeAttr('disabled','disabled');
        addBtn.removeAttr('disabled','disabled');
	}

	function disableElements(){
		selectElement.attr('disabled','disabled');
        resetBtn.attr('disabled','disabled');
        copyBtn.attr('disabled','disabled');
        randomBtn.attr('disabled','disabled');
        checkBox.attr('disabled','disabled');
        addBtn.attr('disabled','disabled');
	}

	function getSetVal(id,property,display){
        var val= $(id).val();
        output.css(property,val);
        $(display).html(val+";");
    }

    var styleCounter=0;
    function styleQueue(){
     	var savedStyleArr= $('#css-code-container').text().split(';'),
     	counter= s.counter(),
     	styleDiv= $('#saved-styles-div');
    	savedStyleArr.pop();
    	s.addString(savedStyleArr,';');
	    s.elementCreation(savedStyleArr,'saved-styles-div','p');
	    styleDiv.find('p').css({
	    	'padding':'.5em',
	    	'margin-bottom':'-3px'
	    });
	    styleDiv.find('p').eq(styleCounter).before("<p class='style-count'>Style "+counter+
	    ":</p>");
	    $('.style-count').css({
	    	'font-size':'1.3em',
	    	'background':'#ECE3DC',
	    	'margin-top':'10px',
	    	'padding':'.5em',
	    	'font-family':"'Lato', sans-serif",
	    	'letter-spacing':'1px',
	    	'font-weight':'700',
	    	'color':'#6A6A68'
	    });
	    styleCounter= styleCounter+14;
    }

    /*output is drag and droppable in larger devices*/
	if(w>767){
	    $("#output").draggable({
		    cursor: "crosshair",   
		    scroll: true,
		    containment: "document",
		    delay: 100
	    });

	    $("#output").draggable( "option" ,"cursor", "crosshair","scroll", true,"containment", "document", "delay", 100 );     
	}

	checkUncheck.on('click', function() {
		var $this= $(this);
		if($this.prop("checked") == false){
            checkBox.prop('checked',false);
            checkLabel.html('Check All');     
        }  
        else if($this.prop("checked") == true){
            checkBox.prop('checked',true);
            checkLabel.html('Uncheck All');      
        }
	});
  	   
	$('p.header-section').addClass('text-center'); /*makes all header text alignment to center*/
	$('code.css-code').find('p').find('span').addClass('css-code-value');/*makes css values red*/
    var output= $('#output');

    $('#input').on('keyup',function(){
        text= $(this).val();
        text= text.replace(/(<([^>]+)>)/ig,""); /*strips html tags*/
        output.html(function(){
        	var mover = "<span class='glyphicon glyphicon-move pull-right' aria-hidden='true'></span>";
        	return text+mover;  /*text+glyphicon in the output*/
        });

        notice.fadeIn('700'); /*dynamic link appears*/
        enableElements();
        cssProperties.fadeIn('700');
        randomCheck.fadeIn('700');
        if(text.length<1){   /*no text!*/
        	cssProperties.fadeOut('700');
        	notice.fadeOut('700');
        	disableElements();
        	randomCheck.fadeOut('700');
        }
    });

    /*$('#input').on('focus',function(){
        $('#input').trigger( "keyup" );
    	$('#input').text("Grumpy wizards make toxic brew for the evil Queen and Jack.");
    });

    $('#text-generator').on('click', function() {
    	$('#input').trigger( "keyup" );
    	$('#input').text("Grumpy wizards make toxic brew for the evil Queen and Jack.");
    });*/

    /*section: creating options for all select menus, getting & setting values on change event*/
    s.elementCreation(fontArr,'fonts','option'); 
    font.change(function() {
	    getSetVal('#fonts','font-family','#font-family-code');
        linkGenerator($(this).val(),'font-link');
	});

    s.elementCreation(sizeArr,'size','option'); 
    size.find('option').eq(4).attr('selected','selected');
	size.change(function() {
		getSetVal('#size','font-size','#font-size-code');
	});

    
    s.elementCreation(styleArr,'style','option'); 
	style.change(function() {
	    getSetVal('#style','font-style','#font-style-code');
	});

    s.elementCreation(weightArr,'weight','option');
	weight.change(function() {
	    getSetVal('#weight','font-weight','#font-weight-code');
	});

    s.elementCreation(vrntArr,'variant','option');
	variant.change(function() {
	    getSetVal('#variant','font-variant','#font-variant-code');
	});

	colorSetter('#color','color','#color-code','span.input-group-addon-color');
    colorSetter('#background','background','#background-code','span.input-group-addon-background');
   
    s.elementCreation(spaceArr,'spacing','option');
	lSpacing.change(function() {
	    getSetVal('#spacing','letter-spacing','#letter-spacing-code');
	});

	s.elementCreation(spaceArr,'word-spacing','option');
	wSpacing.change(function() {
	    getSetVal('#word-spacing','word-spacing','#word-spacing-code');
	});

	s.elementCreation(transformArr,'transform','option');
	transform.change(function() {
	    getSetVal('#transform','text-transform','#text-transform-code');
	});

	s.elementCreation(decorArr,'decoration','option');
	decoration.change(function() {
	    getSetVal('#decoration','text-decoration','#text-decoration-code');
	});

	s.elementCreation(alignArr,'align','option');
	align.change(function() {
	    getSetVal('#align','text-align','#text-align-code');
	});

	s.elementCreation(spaceArr,'padding','option');
	padding.find('option').eq(6).attr('selected','selected');
	padding.change(function() {
	    getSetVal('#padding','padding','#padding-code');
	});
	/***end of section: creating options for all select menus, getting & setting values on change event***/

    /*resets all styling to default*/
	resetBtn.on('click',function(){
		deafaulStyleDisplay();
        defaultStyle();
        /*section: selecting/setting default options*/
        size.find('option').eq(4).attr('selected','selected');
        style.find('option').eq(0).attr('selected','selected');
        font.find('option').eq(0).attr('selected','selected');
        weight.find('option').eq(0).attr('selected','selected');
        variant.find('option').eq(0).attr('selected','selected');
        color.val('#000000');
        lSpacing.find('option').eq(0).attr('selected','selected');
        wSpacing.find('option').eq(0).attr('selected','selected');
        transform.find('option').eq(0).attr('selected','selected');
        decoration.find('option').eq(0).attr('selected','selected');
        align.find('option').eq(0).attr('selected','selected');
        padding.find('option').eq(6).attr('selected','selected');
        background.val('#fffcfc');
        linkGenerator(fontArr[0],'font-link');
        colorAddon.hide('700');
        backgroundAddon.hide('700');
        /***end of section: selecting/setting default options***/
	});

    randomBtn.on('click', function() {
    	
        var  randomSize= $('#font-size-code').text().replace(";",''),
        sizeIndex= sizeArr.indexOf(randomSize),
    	randomStyle= style.val(),
    	styleIndex= styleArr.indexOf(randomStyle),
    	randomFont= font.val(),
    	fontIndex= fontArr.indexOf(randomFont),
    	randomWeight= weight.val(),
    	weightIndex= weightArr.indexOf(randomWeight),
    	randomVariant= variant.val(),
    	variantIndex= vrntArr.indexOf(randomVariant),
    	randomLSpacing= lSpacing.val(),
        lSpaceIndex= spaceArr.indexOf(randomLSpacing),
        randomWSpacing= wSpacing.val(),
        wSpaceIndex= spaceArr.indexOf(randomWSpacing),
        randomTransform= transform.val(),
        transformIndex= transformArr.indexOf(randomTransform),
        randomDecoration= decoration.val(),
        decorationIndex= decorArr.indexOf(randomDecoration),
        randomAlign= align.val(),
        alignIndex= alignArr.indexOf(randomAlign),
        randomPadding= $('#padding-code').text().replace(";",''),
        paddingIndex= spaceArr.indexOf(randomPadding);

        var hexArr=['f','a','b','c','f','d','e','f',1,2,3,4,'f',5,6,'f','f',7,8,9,0];
        function randomColorGen(checkboxId,colorPickerId){
        	var randColor;
            if($(checkboxId).prop("checked") == true){
	            var colorVal="";

		        for(var i=0;i<6;i++){
		        	var randHex= s.randomElement(hexArr);
		        	colorVal+= randHex;
		        }

		        randColor= "#"+colorVal,
		       
	        	colorPickerId.val(randColor);
	            colorPickerId.colorpicker('setValue',randColor);
	        }
	        else if($(checkboxId).prop("checked") == false){
	        	randColor= colorPickerId.val();
	        }
	        return randColor;
        }
       
        var randomColor= randomColorGen('#color-check',color),
        randomBg=randomColorGen('#bg-check',background);
       
        if($('#size-check').prop("checked") == true){
            randomSize= s.randomElement(sizeArr);
            sizeIndex= sizeArr.indexOf(randomSize);
            size.find('option').eq(sizeIndex).attr('selected','selected');
        }
        else if($('#size-check').prop("checked") == false){
        	randomSize= randomSize;
            sizeIndex= sizeIndex;
        }

        if($('#style-check').prop("checked") == true){
            randomStyle= s.randomElement(styleArr);
            styleIndex= styleArr.indexOf(randomStyle);
            style.find('option').eq(styleIndex).attr('selected','selected');
        }
        else if($('#style-check').prop("checked") == false){
        	randomStyle= randomStyle;
        	styleIndex= styleIndex;
        }

        if($('#font-check').prop("checked") == true){
            randomFont= s.randomElement(fontArr);
            fontIndex= fontArr.indexOf(randomFont);
            font.find('option').eq(fontIndex).attr('selected','selected');
        }
        else if($('#font--check').prop("checked") == false){
        	randomFont= randomFont;
        	fontIndex= fontIndex;
        }

        if($('#weight-check').prop("checked") == true){
            randomWeight= s.randomElement(weightArr);
            weightIndex= weightArr.indexOf(randomWeight);
            weight.find('option').eq(weightIndex).attr('selected','selected');
        }
        else if($('#weight-check').prop("checked") == false){
        	randomWeight= randomWeight;
        	weightIndex= weightIndex;
        }

        if($('#variant-check').prop("checked") == true){
            randomVariant= s.randomElement(vrntArr);
            variantIndex= vrntArr.indexOf(randomVariant);
            variant.find('option').eq(variantIndex).attr('selected','selected');
        }
        else if($('#variant-check').prop("checked") == false){
        	randomVariant= randomVariant;
        	variantIndex= variantIndex;
        }

        if($('#l-spacing-check').prop("checked") == true){
            randomLSpacing= s.randomElement(spaceArr);
            lSpaceIndex= spaceArr.indexOf(randomLSpacing);
            lSpacing.find('option').eq(lSpaceIndex).attr('selected','selected');
        }
        else if($('#l-spacing-check').prop("checked") == false){
        	randomLSpacing= randomLSpacing;
        	lSpaceIndex= lSpaceIndex;
        }

        if($('#w-spacing-check').prop("checked") == true){
            randomWSpacing= s.randomElement(spaceArr);
            wSpaceIndex= spaceArr.indexOf(randomWSpacing);
            wSpacing.find('option').eq(wSpaceIndex).attr('selected','selected');
        }
        else if($('#w-spacing-check').prop("checked") == false){
        	randomWSpacing= randomWSpacing;
        	wSpaceIndex= wSpaceIndex;
        }

        if($('#transform-check').prop("checked") == true){
            randomTransform= s.randomElement(transformArr);
            transformIndex= transformArr.indexOf(randomTransform);
            transform.find('option').eq(transformIndex).attr('selected','selected');
        }
        else if($('#transform-check').prop("checked") == false){
        	randomTransform= randomTransform;
        	transformIndex=  transformIndex;
        }

        if($('#decoration-check').prop("checked") == true){
            randomDecoration= s.randomElement(decorArr);
            decorationIndex= decorArr.indexOf(randomDecoration);
            decoration.find('option').eq(decorationIndex).attr('selected','selected');
        }
        else if($('#decoration-check').prop("checked") == false){
        	randomDecoration= randomDecoration;
        	decorationIndex= decorationIndex;
        }

        if($('#align-check').prop("checked") == true){
            randomAlign= s.randomElement(alignArr);
            alignIndex= alignArr.indexOf(randomAlign);
            align.find('option').eq(alignIndex).attr('selected','selected');
        }
        else if($('#align-check').prop("checked") == false){
        	randomAlign= randomAlign;
        	alignIndex= alignIndex;
        }

        if($('#padding-check').prop("checked") == true){
            randomPadding= s.randomElement(spaceArr);
            paddingIndex= spaceArr.indexOf(randomPadding);
            padding.find('option').eq(paddingIndex).attr('selected','selected');
        }
        else if($('#padding-check').prop("checked") == false){
        	randomPadding= randomPadding;
        	paddingIndex= paddingIndex;
        }

    	output.css({
		    'font-size': randomSize,
		    'font-style': randomStyle,
		    'font-family': randomFont,
		    'font-weight': randomWeight,
		    'font-variant': randomVariant,
		    'color': randomColor,
		    'letter-spacing': randomLSpacing,
		    'word-spacing': randomWSpacing,
		    'text-transform': randomTransform,
		    'text-decoration': randomDecoration,
		    'text-align': randomAlign,
		    'background': randomBg,
		    'padding': randomPadding
		});

        $("#font-size-code").html(randomSize+";");
	    $("#font-style-code").html(randomStyle+";");
	    $("#font-family-code").html(randomFont+";");
	    $("#font-weight-code").html(randomWeight+";");
	    $("#font-variant-code").html(randomVariant+";");
	    $("#color-code").html(randomColor+';');
	    $("#letter-spacing-code").html(randomLSpacing+";");
	    $("#word-spacing-code").html(randomWSpacing+";");
	    $("#text-transform-code").html(randomTransform+";");
	    $("#text-decoration-code").html(randomDecoration+";");
	    $("#text-align-code").html(randomAlign+";");
	    $("#background-code").html(randomBg+';');
	    $("#padding-code").html(randomPadding+';');

        linkGenerator(randomFont,'font-link');
    });

    addBtn.on('click',styleQueue);
    addBtn.on('mousedown',function(){
    	var $this=$(this);
    	$this.attr('title','Added!'); /*popover title*/
    	$this.popover('show');
    });

    deafaulStyleDisplay();
    defaultStyle();

    function pxToem(){
        for(var i=0;i<arguments.length;i++){
            var pixelArr=[],
            mainPixel= $(arguments[i]).text(),
            pixelVal= parseInt(mainPixel);
            if(mainPixel.indexOf('em')==-1){
                $(arguments[i]).html(pixelVal*.0625+"em;");
            }
        }   
    }

    $('#px-to-em').on('click', function() {
        pxToem('#font-size-code','#padding-code','#letter-spacing-code','#word-spacing-code');
    });

    /*section: copy to clipboard*/
    copyBtn.on('mousedown',function(){
    	var $this=$(this);
    	$this.attr('title','Copied!'); /*popover title*/
    	$this.popover('show');
    });

    var clipboard = new Clipboard('#copy');
	clipboard.on('success', function(e) {
	    //e.clearSelection();
	});
	/***end of section: copy to clipboard***/
})();



